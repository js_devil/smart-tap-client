import Vue from 'vue';
import VueRouter from 'vue-router';
// import Home from '../views/Home.vue';
import store from '../store/index';
Vue.use(VueRouter);

const authRoutes = [
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import('../views/Dashboard.vue'),
  },
  {
    path: '/users',
    name: 'Users',
    component: () => import('../views/dashboard/Users.vue'),
  },
  {
    path: '/company/account',
    name: 'Settings',
    component: () => import('../views/dashboard/Settings.vue'),
  },
];

const routes = [
  ...authRoutes,
  {
    path: '/',
    name: 'Home',
    // component: Home,
    redirect: '/login',
  },
  {
    path: '/404',
    name: 'NotFound',
    component: () => import('../views/404.vue'),
  },
  { path: '*', redirect: '/404' },

  // Auth Routes
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/auth/Login.vue'),
  },
  {
    path: '/signup',
    name: 'Signup',
    component: () => import('../views/auth/Signup.vue'),
  },
  {
    path: '/forgot-password',
    name: 'ForgotPassword',
    component: () => import('../views/auth/ForgotPassword.vue'),
  },
  {
    path: '/reset-password',
    name: 'ResetPassword',
    component: () => import('../views/auth/ResetPassword.vue'),
  },
  {
    path: '/auth-lock',
    name: 'AuthLock',
    component: () => import('../views/auth/Lock.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

router.beforeEach((to, from, next) => {
  let company = JSON.parse(localStorage.getItem('company'));

  if (authRoutes.map((key) => key.name).includes(to.name) && !company)
    return next({ name: 'Login' });

  if (company)
    store.commit('set', {
      company: {
        ...company,
        loggedIn: true,
      },
    });

  if (to.name == 'Login' && company) return next('/dashboard');

  next();
});

export default router;
