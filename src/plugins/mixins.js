import Alert from '@/components/utils/Alert.vue';
const imageUrl =
  'https://upload.wikimedia.org/wikipedia/commons/0/03/Forbidden_Symbol_Transparent.svg';

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

import { mapState } from 'vuex';
export default {
  computed: {
    ...mapState(['loader', 'company']),
  },
  components: {
    Alert,
  },
  data() {
    return {
      // user_img: require("../assets/img/user.svg"),
      naira: '\u20A6',
      imageUrl: '',
      logo: '',
      loading: false,
      showPass: false,
      showCPass: false,

      showAlert: false,
      alertDetails: {},
      componentKey: 0,
    };
  },
  methods: {
    async getCompanyDetails() {
      this.$store.commit('set', {
        loader: true,
      });

      try {
        const res = await this.$axios({
          url: `/profile`,
          method: 'get',
          headers: {
            Authorization: `Bearer ${this.company.access_token}`,
          },
        });

        const company = {
          ...res.data.data,
          access_token: this.company.access_token,
        };

        this.$store.commit('set', { company, loader: false });
        localStorage.setItem('company', JSON.stringify(company));
        return company;
      } catch (err) {
        this.catchErrors(err);
      }
    },
    turnToLower(a) {
      for (let i in a) {
        a = {
          ...a,
          [i]: a[i] ? `${a[i]}`.toLowerCase() : null,
        };
      }
      return a;
    },

    catchErrors({ response }) {
      this.$store.commit('set', {
        loader: false,
        loadingText: '',
      });
      this.loading = false;
      if (!response)
        return this.$toastr.w(
          'Please check your internet connection and try again',
          'Network Error!'
        );

      if (response.status === 401) {
        localStorage.removeItem('company');
        sessionStorage.clear();
        this.$store.commit('set', {
          company: {
            loggedIn: false,
          },
        });
        this.$router.push('/');
        this.$toastr.i(
          'Log in again to continue using the app',
          'Session Expired!'
        );
        return;
      }

      //   console.log(response);

      const { message, error } = response.data;
      if (message.includes('verify your email'))
        return this.$swal
          .fire({
            title: '<strong>Email Not Verified!</strong>',
            text:
              'Please verify your email and try again. Click the button to resend verification link',
            icon: 'info',
            focusConfirm: false,
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Resend mail!',
            showLoaderOnConfirm: true,
          })
          .then((result) => {
            if (result.value) {
              this.resendMail();
            }
          });

      if (error) return this.showError('', message);
    },

    showFund: () => $('#fundAccount').modal(),
    showWithdraw: () => $('#withdrawAcct').modal(),

    showError(title, text) {
      return this.$swal.fire({
        imageUrl,
        imageAlt: 'forbidden',
        imageHeight: 135,
        title: title || 'Oops!',
        text,
      });
    },

    logout() {
      this.$swal
        .fire({
          title: 'Are you sure?',
          text: 'You are about to log off!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, Log me out',
        })
        .then((result) => {
          if (result.value) {
            localStorage.removeItem('company');
            sessionStorage.clear();
            this.$store.commit('set', {
              company: {
                loggedIn: false,
              },
            });
            this.$router.push({
              name: 'Login',
            });
            this.$toastr.s('See you next time', 'Logged Out!');
          }
        });
    },

    beforeUpload({ type, size }) {
      const isType =
        type === 'image/jpeg' || type === 'image/png' || type === 'image/jpg';

      const imageSize = size / 1024 / 1000;
      if (!isType) {
        this.$toastr.w(
          'You can only upload JPG, JPEG or PNG files!',
          'Warning'
        );

        return false;
      }

      if (imageSize > 0.8) {
        this.$toastr.w(
          'You can only upload images with maximum size of 800kb!',
          'Warning'
        );

        return false;
      }

      return true;
    },
    numberWithCommas: (x) => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','),
    removeCommas: (amount) => amount.replace(/,/g, ''),
    formatAmount: (amount) =>
      amount
        ? parseFloat(parseFloat(amount).toFixed(2)).toLocaleString('en')
        : '0.00',

    // validation
    formatTime(date) {
      let hours = date.getHours();
      let minutes = date.getMinutes();
      let ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0' + minutes : minutes;
      return `${hours}:${minutes} ${ampm}`;
    },
    formatDate: (date) =>
      `${months[new Date(date).getMonth()]} ${new Date(date).getDate()}`,
    validateName: (value) => /^[a-z A-Z]+$/.test(value),
    validatePassword: (value) =>
      /^([0-9]|[a-z])+([0-9a-z]+)$/i.test(value) && value.length > 7,
    validateEmail: (email) => /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email),
    validatePhone: (value) => /^\d+$/.test(value) && value.length,
    validateNumbers: (value) => /^\d+$/.test(value),
    runScripts(src) {
      let main = document.createElement('script');
      main.setAttribute('src', src);
      // /app-assets/js / scripts / pages / dashboard - analytics.js
      document.head.appendChild(main);
    },
  },
  mounted() {
    // this.runScripts();
  },
  watch: {
    $route() {
      // if (this.$route.name.toLowerCase().includes("user")) this.runScripts();
    },
    showAlert() {
      if (this.showAlert)
        setTimeout(() => {
          this.showAlert = false;
        }, 5000);
    },
  },
};
