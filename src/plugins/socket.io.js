import io from "socket.io-client";
import VueSocketIO from "vue-socket.io";
const options = { path: "/requests/" };

export default (baseURL, store) =>
  new VueSocketIO({
    debug: true,
    connection: io(baseURL), //options object is Optional
    vuex: {
      store,
      actionPrefix: "SOCKET_",
      mutationPrefix: "SOCKET_",
    },
  });

// this.$socket.emit("updateLocation", { data: "name" });
