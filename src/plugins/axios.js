import axios from "axios";

export default axios.create({
  baseURL: `https://smartap.herokuapp.com/api/v1/client`,
});