import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loader: false,
    rates: {},
    fees: {},
    loadingText: '',
    company: {
      loggedIn: false,
    },
  },
  mutations: {
    set(state, data) {
      for (let key of Object.keys(data)) state[key] = data[key];
    },
    // update(state, data) {
    //   for (let key of Object.keys(data)) state[key] = data[key];
    //   // for (let key of Object.keys(data)) Vue.set(state.user, key, data[key]);
    // },
  },
  getters: {
    user_image: (state) => state.user.user_details.profile_pic,
  },
  actions: {},
  modules: {},
});
