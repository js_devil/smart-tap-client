import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

import axios from "./plugins/axios";
Vue.prototype.$axios = axios;

import VueToastr from "vue-toastr";
Vue.use(VueToastr, {
  defaultTimeout: 5000,
  defaultCloseOnHover: false,
});

import Swal from "sweetalert2";
Vue.prototype.$swal = Swal;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
